package io.muic.ooc.webapp.service;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bright on 2/23/2018.
 */
public class MessageService {

    public static void messageDispatcher(HttpServletRequest request, HttpServletResponse response, String message, String path) throws ServletException, IOException {
        request.setAttribute("message", message);
        RequestDispatcher rd = request.getRequestDispatcher(path);
        rd.include(request, response);
    }
}
