package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.MySQL;
import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.service.MessageService;
import io.muic.ooc.webapp.service.SecurityService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bright on 2/24/2018.
 */
public class EditProfileServlet extends HttpServlet implements Routable {
    private SecurityService securityService;

    @Override
    public String getMapping() {
        return "/editInfo";
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/edit.jsp");
        rd.include(request, response);
        boolean authorized = securityService.isAuthorized(request);
        if (!authorized) {
            response.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean authorized = securityService.isAuthorized(request);
        if (!authorized) {
            String error = "You need to login first";
            MessageService.messageDispatcher(request,response,error,"WEB-INF/login.jsp");
            response.sendRedirect("/login");
        }

        String id = request.getParameter("user_id");
        String username = request.getParameter("username");
        String password = request.getParameter("new_password");
        String confirmpwd = request.getParameter("new_confirmpwd");
        String fname = request.getParameter("new_fname");
        String lname = request.getParameter("new_lname");
        String email = request.getParameter("new_email");

        if (MySQL.isDataExist("Email",email)){
            String path = "user?edit=fail&&user="+username+"&&by=email";
            response.sendRedirect(path);
//            MessageService.messageDispatcher(request,response,error,"WEB-INF/edit.jsp");
        }
        else if(!confirmpwd.equals(password)){
            String path = "user?edit=fail&&user="+username+"&&by=pwd";
            response.sendRedirect(path);
        }
        else if(!StringUtils.isAlphanumeric(username) ||
                !StringUtils.isAlphanumeric(password) ||
                !StringUtils.isAlphanumeric(confirmpwd)){
            String path = "user?edit=fail&&user="+username+"&&by=invalid";
            response.sendRedirect(path);
        }
        else {
            if (!StringUtils.isBlank(fname)) {
                MySQL.updateData("FirstName", id, fname);
            }

            if (!StringUtils.isBlank(lname)) {
                MySQL.updateData("LastName", id, lname);
            }
            if (!StringUtils.isBlank(email) ) {
                MySQL.updateData("Email", id, email);
            }

            if (!StringUtils.isBlank(password)) {
                MySQL.updateData("Password", id, password);
            }
            String path = "/user?edit=success&&user="+username;
            response.sendRedirect(path);
        }

    }
}
