package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.MySQL;
import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.service.MessageService;
import io.muic.ooc.webapp.service.SecurityService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bright on 2/17/2018.
 */
public class RegisterServlet extends HttpServlet implements Routable {

    private SecurityService securityService;


    @Override
    public String getMapping() {
        return "/register";
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String confirmpwd = request.getParameter("confirmpwd");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");


        if (StringUtils.isBlank(username) ||
                StringUtils.isBlank(password) ||
                !StringUtils.isAlphanumeric(username) ||
                !StringUtils.isAlphanumeric(password) ){
            String error = "Username or Password must not leave blank, or your Username or Password is invalid";
            MessageService.messageDispatcher(request,response,error,"WEB-INF/register.jsp");
        }
        else if(MySQL.isDataExist("username",username)){
            String error = "This username is already used";
            MessageService.messageDispatcher(request,response,error,"WEB-INF/register.jsp");
        }
        else if(!password.equals(confirmpwd)) {
            String error = "Password Confirmation is mismatch";
            MessageService.messageDispatcher(request, response, error,"WEB-INF/register.jsp");
        }
        else if(MySQL.isDataExist("email",username) &&
                StringUtils.isBlank(email)){
            String error = "This email is already used, and Email must not leave empty";
            MessageService.messageDispatcher(request,response,error,"WEB-INF/register.jsp");
        }

        else{
            MySQL.createNewUser(username,password,fname,lname,email);
            response.sendRedirect("/login?register=success");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/register.jsp");
        rd.include(request, response);
    }
}
