package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.service.MessageService;
import io.muic.ooc.webapp.service.SecurityService;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import io.muic.ooc.webapp.Routable;

public class LoginServlet extends HttpServlet implements Routable {

    private SecurityService securityService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/login.jsp");
        rd.include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // do login post logic
        // extract username and password from request
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (!StringUtils.isBlank(username) &&
                !StringUtils.isBlank(password) &&
                StringUtils.isAlphanumeric(username) &&
                StringUtils.isAlphanumeric(password)) {
            if (securityService.authenticate(username, password, request)) {
                response.sendRedirect("/user");
            } else {
                String error = "Wrong username or password.";
                MessageService.messageDispatcher(request,response,error,"WEB-INF/login.jsp");
            }
        } else {
            String error = "Username or password is missing, or Invalid username or password";

            MessageService.messageDispatcher(request,response,error,"WEB-INF/login.jsp");
        }
    }

    @Override
    public String getMapping() {
        return "/login";
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
