package io.muic.ooc.webapp;

import io.muic.ooc.webapp.entity.User;
import org.apache.commons.lang.StringUtils;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * Created by bright on 2/20/2018.
 */
public class MySQL {
    private static Connection connection;
    private static PreparedStatement statement;
    private static ResultSet resultSet;
    private static final String dbSchema = "ooc_db";
    private static final String dbUser = "bright1h";
    private static final String dbPassword ="Bright11-sql";
    private static final String dbAddress="jdbc:mysql://localhost:3306/"+ dbSchema+"?useSSL=false";
    private static final String dbTable = "users";


    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            if (connection == null || connection.isClosed()) {
                connection = DriverManager.getConnection(dbAddress, dbUser, dbPassword);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        if (connection == null) {
            System.out.println("Connection is NULL!");
        }
        return connection;
    }

    public static boolean createTable(){
        try {
            String sql_table = "CREATE TABLE users (" +
                    "    ID int NOT NULL AUTO_INCREMENT," +
                    "    Username varchar(255) NOT NULL," +
                    "    Password varchar(255) NOT NULL," +
                    "    FirstName varchar(255) NOT NULL," +
                    "    LastName varchar(255) NOT NULL," +
                    "    Email varchar(255) NOT NULL," +
                    "    UNIQUE (Username)," +
                    "    PRIMARY KEY (ID)" +
                    ");";
            statement = getConnection().prepareStatement(sql_table);
            statement.executeUpdate();
            return true;
        }catch (SQLException e) {
            System.out.println("Table is already created");
            return false;
        }
        finally {
            try {
                if (!connection.isClosed())connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isDataExist(String Column, String uname){
        String sql ="SELECT " + Column +" FROM "+
                dbTable +
                " WHERE Username =?";

        try {
            statement = getConnection().prepareStatement(sql);
            statement.setString(1,uname);

            resultSet = statement.executeQuery();
            System.out.println("in userExist resultSet: " + resultSet);
            if (resultSet.first()){
                return true; //user existed
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (!connection.isClosed())connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }


    public static boolean createNewUser(String uname,String password, String fname, String lname, String email){

        String sql = "INSERT INTO users (Username,Password,FirstName,LastName,Email)" +
                "VALUES (?,?,?,?,?);";
        String hashPwd= BCrypt.hashpw(password, BCrypt.gensalt());

            try {
                statement = getConnection().prepareStatement(sql);
                statement.setString(1, uname);
                statement.setString(2, hashPwd);
                statement.setString(3, fname);
                statement.setString(4, lname);
                statement.setString(5, email);
                statement.executeUpdate();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false; //Fail to add user
            } finally {
                try {
                    if (!connection.isClosed())connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
    }

    public static boolean authenticate(String uname, String password) {
        User user = getUser(uname);
        if (user !=null) {
            if (user.getUsername().equals(uname)) {
                return BCrypt.checkpw(password, user.getPassword());
            }
        }

        return false;
    }

    public static boolean updateData(String column,
                                     String userID,
                                     String updateValue){
        String sql = "UPDATE " + dbTable +
                " SET "+ column+
                " = ? WHERE ID = ?";
        try {
            if (column.equals("Password")){
                updateValue = BCrypt.hashpw(updateValue, BCrypt.gensalt());
            }
            statement = getConnection().prepareStatement(sql);
            statement.setString(1,updateValue);
            statement.setString(2,userID);
            System.out.println(statement.toString());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (!connection.isClosed())connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean removeUser(String userID){
        String sql = "DELETE FROM " +
                dbTable +
                " WHERE ID=?";
        try {
            statement = getConnection().prepareStatement(sql);
            statement.setString(1,userID);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (!connection.isClosed())connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static User getUser(String username){
        User user = new User();
        String sql = "SELECT * FROM " +
                dbTable +
                " WHERE username = ?";

        try {
            statement = getConnection().prepareStatement(sql);
            statement.setString(1,username);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                user.setId(resultSet.getInt("id"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                user.setFirstname(resultSet.getString("firstname"));
                user.setLastname(resultSet.getString("lastname"));
                user.setEmail(resultSet.getString("email"));

                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (!connection.isClosed())connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static List<User> getUserList(){
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM "+ dbTable;
        try {
            statement = getConnection().prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                int userID = resultSet.getInt("ID");
                String uname = resultSet.getString("username");
                String pwd = resultSet.getString("password");
                String fname = resultSet.getString("firstname");
                String lname = resultSet.getString("lastname");
                String email = resultSet.getString("email");
                User user = new User(userID,uname,pwd,fname,lname,email);
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (!connection.isClosed())connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return users;
    }


}
