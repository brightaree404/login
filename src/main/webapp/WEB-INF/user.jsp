<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="io.muic.ooc.webapp.MySQL" %>
<%@ page import="io.muic.ooc.webapp.entity.User" %>
<%@ page import="java.util.List" %>

<html>
    <head>
        <title>User Management</title>
    </head>
    <body>
        <script type='text/javascript'>
            function confirmDel() {
                return confirm("Are you sure to delete this user?");
            }
        </script>

        <h2>Welcome, ${username}</h2>

        <%--Message Status--%>
        <%

        if (request.getParameter("edit")!=null){
            if (request.getParameter("edit").equals("success")){
                %> <%=request.getParameter("user")%>'s profile is updated<%
            }

            else if(request.getParameter("edit").equals("fail")){
                System.out.println("fail");
                if (request.getParameter("by").equals("email")){
                    %> <%=request.getParameter("user")%>'s profile failed to update : The Email is already existed<%

                }
                else if (request.getParameter("by").equals("pwd")){
                    %> <%=request.getParameter("user")%>'s profile failed to update : Confirm password is not matched<%
                }
                else if (request.getParameter("by").equals("invalid")){
                    %> <%=request.getParameter("user")%>'s profile failed to update : Invalid password<%
                }
            }
        }

        %>


        <table border="1">
            <tr>
                <td>ID</td>
                <td>Username</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Email</td>
                <td>Functions</td>
            </tr>
            <%

                String currentUser = (String) request.getSession().getAttribute("username");
                List<User> userList = MySQL.getUserList();

                for (int i=0;i<userList.size();i++)
                {
                    User user = userList.get(i);
            %>
            <tr>
                <td><%=user.getId()%></td>
                <td><%=user.getUsername()%></td>
                <td><%=user.getFirstname()%></td>
                <td><%=user.getLastname()%></td>
                <td><%=user.getEmail()%></td>
                <td>
                    <form action="/editInfo" method="GET">
                        <input type="hidden" name="editUser" value="<%=user.getUsername()%>" >
                        <input type="submit" value="Edit">
                    </form>
                <%

                if(user.getUsername().equals(currentUser)){
                %>
                    <input type="submit" value="Delete" disabled="disabled">
                <%
                }else{
                %>
                    <form onsubmit="return confirmDel()" action="/remove" method="GET">
                        <input type="hidden" name="id" value="<%=user.getId()%>" >
                        <input type="submit" value="Delete">
                    </form>

                <%
                }
                %>

                </td>
            </tr>
            <%
            }
            %>
        </table>
        <form action="/logout" method="GET">
            <input type="submit" value="Logout">
        </form>

        <form action="/register" method="GET">
            <input type="submit" value="Sign Up">
        </form>

    </body>
</html>
