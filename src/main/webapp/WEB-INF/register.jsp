<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
    <body>
        <h2>Register Page</h2>
        <p>${message}</p>
        <form action="/register" method="post">
            Username:
            <input type="text" name="username" placeholder="Username"   />
            <br/>

            First Name:
            <input type="text" name="fname" placeholder="First Name"   />
            <br/>

            Last Name:
            <input type="text" name="lname" placeholder="Last Name"  />
            <br/>

            Password:
            <input type="password" name="password" placeholder="Password" />
            <br/>
            Confirm Password:
            <input type="password" name="confirmpwd" placeholder="Confirm Password"/>
            <br/>

            E-mail:
            <input type="email" name = "email" placeholder="E-mail" >
            <br/>

            <br><br>
            <input type="submit" value="Submit">
        </form>

        <form action="/index.jsp" method="GET">
            <input type="submit" value="Cancel">
        </form>

    </body>
</html>
