<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="io.muic.ooc.webapp.MySQL" %>
<%@ page import="io.muic.ooc.webapp.entity.User" %>

<html>
    <body>
        <h2>Edit Profile</h2>
        <p>${message}</p>
        <form action="/editInfo" method="post">
            <table border="1" cellpadding="2">

                <%
                    String editUser = request.getParameter("editUser");
                    System.out.println(editUser);
                    User user = MySQL.getUser(editUser);

                %>

                <tr>
                    <td>
                        Field
                    </td>

                    <td>
                        Information
                    </td>

                    <td>
                        Edit Field
                    </td>
                </tr>


                <tr>
                    <td>
                        ID
                    </td>

                    <td>
                        <%=user.getId()%>

                    </td>

                    <td>
                        Unavaialble
                        <input type="hidden" name="user_id" value="<%=user.getId()%>" >
                    </td>
                </tr>

                <tr>
                    <td>
                        Username
                    </td>

                    <td>
                        <%=user.getUsername()%>
                    </td>

                    <td>
                        Unavailable
                        <input type="hidden" name="username" value="<%=user.getUsername()%>" >
                    </td>

                </tr>

                <tr>
                    <td>
                        FirstName
                    </td>

                    <td>
                        <%=user.getFirstname()%>
                    </td>

                    <td>
                        New FirstName<br>
                        <input type="text" name="new_fname" placeholder="First Name"   />
                    </td>

                </tr>

                <tr>
                    <td>
                        LastName
                    </td>

                    <td>
                        <%=user.getLastname()%>
                    </td>

                    <td>
                        New LastName: <br>
                        <input type="text" name="new_lname" placeholder="Last Name"   />
                    </td>
                </tr>

                <tr>
                    <td>
                        Email
                    </td>

                    <td>
                        <%=user.getEmail()%>
                    </td>

                    <td>
                        New Email:<br>
                        <input type="email" name = "new_email" placeholder="E-mail" >
                    </td>
                </tr>

                <tr>
                    <td>
                        Password
                    </td>

                    <td>
                        Secret
                    </td>
                    <td>
                        New Password:<br>
                        <input type="password" name="new_password" placeholder="Password" /><br>

                        Confirm new Password:<br>
                        <input type="password" name="new_confirmpwd" placeholder="Confirm Password"/>

                    </td>
                </tr>

            </table>
            <input type="submit" value="Submit">
        </form>
        <form action="/user" method="GET">
            <input type="submit" value="Cancel">
        </form>
    </body>
</html>
